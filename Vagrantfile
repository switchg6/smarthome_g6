Vagrant.configure("2") do |config|
  config.vm.box = "envimation/ubuntu-xenial"

  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install python3 --yes
    ifconfig
  SHELL

  #db for the H2 database
  config.vm.define "db" do |db|
    db.vm.box = "envimation/ubuntu-xenial"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.33.12"
	
    # We want to access H2 console from the host using port 8082
    # We want to connet to the H2 server using port
	#db.vm.network "forwarded_port", guest: 8082, host: 8082
    db.vm.network "forwarded_port", guest: 9092, host: 9092

    # To connect to H2 use: jdbc:h2:tcp://192.168.33.12:8443/~/smarthome
    # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
    db.vm.provision "shell", :run => 'always', inline: <<-SHELL
      # So that the H2 server always starts
      java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
    SHELL
 end

  #app for the SmartHome app
  config.vm.define "app" do |app|
    app.vm.box = "envimation/ubuntu-xenial"
    app.vm.hostname = "app"
    app.vm.network "private_network", ip: "192.168.33.11"
    app.vm.network "forwarded_port", guest: 8082, host: 8882
    app.vm.network "forwarded_port", guest: 8443, host: 8843
    app.vm.provision "shell", inline: <<-SHELL
       sudo apt-get install openjdk-8-jdk-headless -y
       sudo apt-get install git -y
    SHELL
    app.vm.provision "shell", :run => 'always', inline: <<-SHELL
       sudo cp /smarthome-1.0-SNAPSHOT.jar /home/smarthome-1.0-SNAPSHOT.jar
       sudo java -jar /home/smarthome-1.0-SNAPSHOT.jar > /dev/null &
    SHELL
  end

  #ui for the React app
  config.vm.define "ui" do |ui|
    ui.vm.box = "envimation/ubuntu-xenial"
    ui.vm.hostname = "ui"
    ui.vm.network "private_network", ip: "192.168.33.13"	
  end

  #ansible for Ansible and Jenkins
  config.vm.define "ansible" do |ansible|
    ansible.vm.box = "envimation/ubuntu-xenial"
    ansible.vm.hostname = "ansible"
    ansible.vm.network "private_network", ip: "192.168.33.10"
    ansible.vm.network "forwarded_port", guest: 8083, host: 9003

    ansible.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=755,fmode=755"]
    ansible.vm.provider "virtualbox" do |v|
    # We need 2048Mb so that SmartHome can execute
      v.memory = 2048
    end

    ansible.vm.provision "shell", inline: <<-SHELL
      sudo apt-get install -y --no-install-recommends apt-utils
      sudo apt-get install software-properties-common --yes
	  
	  #Install Ansible
      sudo apt-add-repository --yes --u ppa:ansible/ansible
      sudo apt-get install ansible --yes
	  sudo apt-get install -y openjdk-8-jdk-headless
	  
	  #Install Git
	  sudo apt install git  --yes
	  
	  #Installl Maven
	  apt-cache search maven
	  sudo apt-get install maven -y
	  
	  #Install NodeJs
	  sudo apt-get install git curl -y
	  curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
	  sudo apt-get install -y nodejs
	  npm install -g create-react-app

      #Docker install
      sudo apt-get remove docker docker-engine docker.io containerd runc
      sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
      curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
      sudo add-apt-repository --yes "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
      sudo apt-get update
      sudo apt-get install -y docker-ce docker-ce-cli containerd.io
      sudo usermod -a -G docker vagrant

	  #Install Jenkins
      wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
      sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
      sudo apt-get update
      sudo apt-get install -y jenkins
      sed -i 's#HTTP_PORT=8080#HTTP_PORT=8083#g' /etc/default/jenkins
      #systemctl status jenkins
      #sudo usermod -aG docker jenkins
      sudo service jenkins restart
      sudo cat /var/lib/jenkins/secrets/initialAdminPassword
    SHELL
  end

end

