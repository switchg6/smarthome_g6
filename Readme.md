SMARTHOME PROJECT - GROUP 6
===================

## Requirements
Implement a pipeline in Jenkins for the SmartHome project with several DevOps concerns:


**1.** Reproduce the solution in a system with Vagrant and a clone of the repository

**2.** Create tasks and issues for all team members 

**3.** Use branches to develop specific features of the solution

**4.** Use Maven as Build Tool

**5.** Use Vagrant to virtualize all the parts of the solutions

**6.** Build the application, generate and publish javadoc, execute tests and publish its results, publish the artifacts, all with a Jenkins pipeline

**7.** In the pipeline, build Docker images and publish them in Docker Hub

**8.** In the pipeline, use Ansible to deploy and configure the application (app and ui) and its database

The first step was to create a new repository with the SmartHome project and give access to the teachers. 

## Git

### Version Control with Git

Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later. 

With Git, every time there is a commit, or a save of the state of the project, Git basically takes a picture of what all files look like at that moment and stores a reference to that snapshot. To be efficient, if files have not changed, Git does not store the file again, just a link to the previous identical file it has already stored. Git thinks about its data more like a stream of snapshots.

One of the biggest advantages of Git is that it is hard to get the system to do anything that is not undoable or to make it erase data in any way. As with any VCS, you can lose or mess up changes you have not committed yet, but after you commit a snapshot into Git, it is very difficult to lose, especially if you regularly push your database to another repository.

Another great benefit is that the way Git branches is incredibly lightweight, making branching operations nearly instantaneous, and switching back and forth between branches generally just as fast. Unlike many other VCSs, Git encourages workflows that branch and merge often.


## Build Tools

Build tools are programs that automate the creation of executable applications from source code. Building incorporates compiling, linking and packaging the code into a usable or executable form. Part of the function of the build tool is also to cope with errors in the compilation process of complex software systems. 

Basically build automation is the act of scripting or automating a wide variety of tasks that software developers do in their day-to-day activities. Using an automation tool allows the build process to be more consistent.

Modern build tools go further in enabling work flow processing by obtaining source code, deploying executables to be tests and even optimizing complex build processes using distributed build technologies, which involves running the build process in a coherent, synchronized manner across several machines.


### Git and Bitbucket

To work on building the project with Maven, a new branch has to be created with the following Git command:

    $ git branch build

Then, checkout to the recently created branch: 

    $ git checkout build

And update the repository to receive the branch:

    $ git push --set-upstream origin build

### Building With Maven

To build with Maven just navigate o to the application's folder and run the following commands from your favourite CLI.

#### Running unit Tests

    $ mvn test

#### Generating the javadoc for source code

    $ mvn javadoc:javadoc

This generates the source code javadoc in the folder "target/site/apidocs/index.html".

#### Generating the javadoc for test cases

    $ mvn javadoc:test-javadoc

This generates the test cases javadoc in the folder "target/site/testapidocs/index.html".

#### Generating the build as an executable .JAR file

    $ mvn package

This generates the build in folder "target". It should be called _smarthome-1.0-SNAPSHOT.jar_

#### Doing it all at once
To run all of the above goals at once just combine them like so:

    $ mvn test javadoc:javadoc javadoc:test-javadoc package

#### Running the build
In your "target" folder run the following command.

    $ java -jar smarthome-1.0-SNAPSHOT.jar

#### Merging

To finish this stage, merge the branch with master using the following commands:
```
$ git checkout master
$ git merge origin/build
$ git push
```

### Building With Gradle  
The approach for building a project using Gradle is very similar to the previous, using Maven.
To build with Gradle just navigate to the application's folder and as previously just run the required commands.
Firstly it is needed to install the Gradle in the project's host machine in order to proceed with the following tasks. 

**Convert Maven build to Gradle**, through an analysis of the pom.xml to collect all the dependencies used in the Maven project. Doing this, several gradle configuration files will be created.
> gradle init

As said befoe, when running the previous commands, some files such as *settings.gradle* and *build.gradle* will be created. We will now look into these two important files.

 - ***Settings.gradle***, which refers the project root package name
 ```
  /*  
   *This file was generated by the Gradle 'init' task. 
   */  
 rootProject.name = 'smarthome'
 ```

- ***build.gradle***, which refers to the whole project dependency and plugins list (such as pom.xml for Maven builds)
```
    /*  
     * This file was generated by the Gradle 'init' task. 
     */
     plugins {  
        id 'java'  
      id 'maven-publish'  
      id 'java-library'  
      id 'org.springframework.boot' version '2.1.3.RELEASE'  
    }  
      
    apply plugin: 'io.spring.dependency-management'  
      
    repositories {  
        mavenLocal()  
        maven {  
            url = 'http://repo.maven.apache.org/maven2'  
      }  
    }  
      
    dependencies {  
        compile 'org.springframework.boot:spring-boot-starter-data-rest:2.0.5.RELEASE'  
      compile 'org.springframework.boot:spring-boot-starter-data-jpa:2.0.5.RELEASE'  
      //compile 'com.h2database:h2:1.4.199'  
      compile 'org.springframework.boot:spring-boot-starter-web:2.0.5.RELEASE'  
      compile 'org.springframework.boot:spring-boot-starter-hateoas:2.1.4.RELEASE'  
      compile 'com.googlecode.json-simple:json-simple:1.1.1'  
      compile 'org.springframework.boot:spring-boot-starter-security:2.0.5.RELEASE'  
      compile 'com.auth0:java-jwt:3.4.1'  
      compile 'org.jetbrains:annotations:16.0.2'  
      compile 'log4j:log4j:1.2.17'  
      compile 'org.modelmapper:modelmapper:2.3.3'  
      runtime 'com.h2database:h2:1.4.197'  
      testCompile 'org.springframework.boot:spring-boot-starter-test:2.0.5.RELEASE'  
      testCompile 'org.junit.jupiter:junit-jupiter-api:5.3.2'  
      testCompile 'org.junit.jupiter:junit-jupiter-engine:5.3.2'  
      testCompile 'org.xmlunit:xmlunit-core:2.6.2'  
      testCompile 'org.xmlunit:xmlunit-matchers:2.6.2'  
    }  
      
    group = 'switch2018.project'  
    version = '1.0-SNAPSHOT'  
    description = 'smarthome'  
    sourceCompatibility = '1.8'  
      
    /*The vast majority of software projects build something that aims to be consumed in some way. It could be a library  
    that other software projects use or it could be an application for end users. Publishing is the process by which  
    the thing being built is made available to consumers.  
    For example, a publication destined for a Maven repository includes, such as our project, one or more artifacts  
    — typically built by the project — plus a POM file describing the primary artifact and its dependencies.  
    The primary artifact is typically the project’s production JAR and secondary artifacts might consist of "-sources"  
    and "-javadoc" JARs.*/  
    publishing {  
        publications {  
            maven(MavenPublication) {  
                from(components.java)  
            }  
        }  
    }  
      
    //task to run JUnit 5 tests  
    test {  
        useJUnitPlatform()  
    }  
      
    //task to compile Java source files.  
    tasks.withType(JavaCompile) {  
        options.encoding = 'UTF-8'  
    }  
      
    //task to Assemble the production JAR file, based on the classes and resources attached to the main source set.  
    //the build .jar file will be located under build/libs/  
    jar {  
        manifest {  
            attributes(  
                    'Class-Path': 'smarthome_g6/src/main/java','Main-Class': 'smarthome.Application'  
      )  
        }  
        from configurations.compile  
        into''  
    }
```
    
**Compile and Test** the Java Project into a build directory and package it in a .jar file  
> gradle build  
  
**Running unit Tests**  
> gradle test  

**Generating the javadoc for source code**  
> gradle javadoc
  
Generates API documentation for the production Java source using Javadoc.
Results located in the folder "build/docs/javadocs/index.html".  
  
**Generating the build as an executable .JAR file**  
> gradle assemble

While *gradle build* will assemble your artifacts with additional checks, *gradle assemble* will only build your artifacts.
  

This generates the build artifact in the folder *build/libs*. It should be called _smarthome-1.0-SNAPSHOT.jar_  
  
## Infrastructure as Code

![infrastructure_as_code](Diagram.png "Infrastructure as Code")
The diagram shown above represents the structure created with all the required tools.
It shows the connection between the virtual machines created with Vagrant and what runs inside each one: one with the application, other with the H2 database, a third one with the UI and the last one with Jenkins, Docker and Ansible.
Also, regarding Jenkins, it lists the different stages of its pipeline. And about Docker it lists the 3 generated images.

## Vagrant
### Virtualization with Vagrant

Virtualization is the process of running a virtual instance of a computer system in a layer abstracted from the actual hardware. The main goal of virtualization is to manage workloads by radically transforming traditional computing to make it more scalable.

Vagrant is an open-source tool for building and managing virtual machine environments in a single workflow. Machines are provisioned on top of VirtualBox, VMware or any other provider. Vagrant offers easy to configure, reproducible, and portable work environments built on top of industry-standard technology and controlled by a single consistent workflow to help maximize the productivity and flexibility of a team.

### Git and Bitbucket

To work on the virtualization, a new branch has to be created with the following Git command:
    
    $ git branch vagrant

Then, checkout to the recently created branch: 

    $ git checkout vagrant

And update the repository to receive the branch:

    $ git push --set-upstream origin vagrant

Every update implies a new commit:

    $ git commit -m 'Update vagrantfile with new db'

### Vagrant Setup

Use Vagrant to setup a virtual environment to execute the SmartHome application.

Create and provision 4 VMs using a Vagrantfile:
 - **db**: H2 server database
 - **app**: SmartHome application
 - **ui**: React application
 - **ansible**: Ansible and Jenkins
 
### Create a Vagrantfile

In a folder call "vagrant" at the root of the project add the smarthome-1.0-SNAPSHOT.jar file and create de following Vagrantfile:

```
Vagrant.configure("2") do |config|
  config.vm.box = "envimation/ubuntu-xenial"

  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install -y avahi-daemon libnss-mdns
    sudo apt-get install python3 --yes
    ifconfig
  SHELL

  #db for the H2 database
  config.vm.define "db" do |db|
    db.vm.box = "envimation/ubuntu-xenial"
    db.vm.hostname = "db"
    db.vm.network "private_network", ip: "192.168.33.12"
	
    # We want to access H2 console from the host using port 8082
    # We want to connet to the H2 server using port
	#db.vm.network "forwarded_port", guest: 8082, host: 8082
    db.vm.network "forwarded_port", guest: 9092, host: 9092

    # To connect to H2 use: jdbc:h2:tcp://192.168.33.12:8443/~/smarthome
    # How to setup java as a service in ubuntu: http://www.jcgonzalez.com/ubuntu-16-java-service-wrapper-example
    db.vm.provision "shell", :run => 'always', inline: <<-SHELL
      # So that the H2 server always starts
      java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > ~/out.txt &
    SHELL
 end

  #app for the SmartHome app
  config.vm.define "app" do |app|
    app.vm.box = "envimation/ubuntu-xenial"
    app.vm.hostname = "app"
    app.vm.network "private_network", ip: "192.168.33.11"
    app.vm.network "forwarded_port", guest: 8082, host: 8082
    app.vm.network "forwarded_port", guest: 8443, host: 8443
    app.vm.provision "shell", inline: <<-SHELL
       sudo apt-get install openjdk-8-jdk-headless -y
       sudo apt-get install git -y
    SHELL
    app.vm.provision "shell", :run => 'always', inline: <<-SHELL
       sudo cp /smarthome-1.0-SNAPSHOT.jar /home/smarthome-1.0-SNAPSHOT.jar
       sudo java -jar /home/smarthome-1.0-SNAPSHOT.jar > /dev/null &
    SHELL
  end

  #ui for the React app
  config.vm.define "ui" do |ui|
    ui.vm.box = "envimation/ubuntu-xenial"
    ui.vm.hostname = "ui"
    ui.vm.network "private_network", ip: "192.168.33.13"	
  end

  #ansible for Ansible and Jenkins
  config.vm.define "ansible" do |ansible|
    ansible.vm.box = "envimation/ubuntu-xenial"
    ansible.vm.hostname = "ansible"
    ansible.vm.network "private_network", ip: "192.168.33.10"
    ansible.vm.network "forwarded_port", guest: 8083, host: 9003

    ansible.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=755,fmode=600"]
    ansible.vm.provider "virtualbox" do |v|
    # We need 2048Mb so that SmartHome can execute
      v.memory = 2048
    end

    ansible.vm.provision "shell", inline: <<-SHELL
      sudo apt-get install -y --no-install-recommends apt-utils
      sudo apt-get install software-properties-common --yes
	  
	  #Install Ansible
      sudo apt-add-repository --yes --u ppa:ansible/ansible
      sudo apt-get install ansible --yes
	  sudo apt-get install -y openjdk-8-jdk-headless
	  
	  #Install Git
	  sudo apt install git  --yes
	  
	  #Installl Maven
	  apt-cache search maven
	  sudo apt-get install maven -y
	  
	  #Install NodeJs
	  sudo apt-get install git curl -y
	  curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
	  sudo apt-get install -y nodejs
	  npm install -g create-react-app

	  #Install Jenkins
      wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
      sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
      sudo apt-get update
      sudo apt-get install -y jenkins
      sed -i 's#HTTP_PORT=8080#HTTP_PORT=8083#g' /etc/default/jenkins
      #systemctl status jenkins
      #sudo usermod -a -G docker jenkins
      sudo service jenkins restart
      sudo cat /var/lib/jenkins/secrets/initialAdminPassword
    SHELL
  end

end
``` 

Execute the command:

    % vagrant up
    
This creates and configures the VMs according to the Vagrantfile.

This VMs will be configure by the Ansible Playbook (last step), so test it and then destroy the VMs:

    $ vagrant destroy –f  
 
To finish this stage, merge the branch with master using the following commands:

```
$ git checkout master
$ git merge origin/vagrant
$ git push
``` 

## Jenkins
### Continuous Integration and Jenkins

Continuous Integration is a development practice that requires developers to integrate code into a shared repository several times a day. Each check-in is then verified by an automated build, allowing teams to detect problems early. 

By integrating regularly, you can detect errors quickly, and locate them more easily. Automation is key to create an efficient and repeatable process.

Jenkins is an open source Continuous Integration tool written in Java. It provides an easy-to-use system, easier for developers to integrate project changes, and easier to obtain a fresh build. The automated, continuous build increases the productivity. 

It is also highly configurable and the community plugins offer more flexibility.


### Git and Bitbucket

To work on the Jenkins pipeline, a new branch has to be created with the following Git command:

`$ git branch pipeline-docker`

Then, checkout to the recently created branch: 

`$ git checkout pipeline-docker`

And update the repository to receive the branch:

`$ git push --set-upstream origin pipeline-docker`

### Installing Jenkins

Jenkins was installed through the inline Shell execution on the set up of VM configuration, 

      #Vagrant file excerpt related to Jenkins
 
      wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
      sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
      
      sudo apt-get update
      sudo apt-get install -y jenkins
      
      sed -i 's#HTTP_PORT=8080#HTTP_PORT=8083#g' /etc/default/jenkins
      
      #To check jenkins status
      #systemctl status jenkins
      
      #To give docker permissions to jenkins to access 
      #sudo usermod -aG docker jenkins
      
      sudo service jenkins restart
      
      #To discover the admin Password to place in the initial login, once VM setup is complete
      #sudo cat /var/lib/jenkins/secrets/initialAdminPassword



After running this set of commands, Jenkins setup status could be verified through browser access in the host machine in `http://192.168.33.10:8083`, that starts by asking for the Admin password.  
  
This password can be obtained by accessing configuration file  
  
> sudo cat /var/lib/jenkins/secrets/initialAdminPassword  
  
After applying the found user key in the login page prompted before, the final step of Jenkins installation is to set up the following parameters:

 1. Plugins
	- JUnit
	- JaCoCo
	- HTML
	- Publisher
	- Ansible
	- Docker
2. User credentials
	- bitbucket-credentials
	- docker-credentials
3. New job "*SmartHome-G6*"
4. Pipeline set up


### Creating the Pipeline

Next, we need to create a pipeline.

From the main page of Jenkins, select "New Item" and select "Pipeline".

On the configuration page of the job, in the pipeline section, select "Pipeline script from SCM" on the "Definition" dropdown. Then, in the "Repositories" section, add the URL to the Bitbucket repository and the credentials added previously to let Jenkins access it.

The "Script Path" indicates the relative location of the Pipeline script: smarthome_g6/Jenkinsfile.

This concludes the pipeline creation. The next step is to create the Jenkinsfile with the pipeline script.

### Compose Jenkinsfile

On the smarthome_g6 project folder, add a Jenkinsfile with all the necessary stages:

```
pipeline {
    agent any

    stages {
        
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'apmc-bitbucket-credentials', url: 'https://1181688@bitbucket.org/switchg6/smarthome_g6'
            }
        }
        
        stage('Build') {
            steps {
                echo 'Building...'
                sh 'mvn clean package'
            }
        }
        
        stage ('Javadoc'){
            steps {
            echo 'Publishing Documentation...'
                sh 'mvn javadoc:javadoc'
                publishHTML([
                            reportName: 'Javadoc',
                            reportDir: 'target/site/apidocs',
                            reportFiles: 'index.html',
                            keepAll: false,
                            alwaysLinkToLastBuild: false,
                            allowMissing: false
                            ])
            }
        }
        
        stage ('Test'){
            steps {
             echo 'Testing...'
                sh 'mvn test'
       		    junit 'target/surefire-reports/*.xml'
                jacoco (execPattern: 'target/jacoco.exec')
            }
        }
        
        stage('Archive') {
            steps {
                echo 'Archiving...'
                 archiveArtifacts 'target/smarthome-1.0-SNAPSHOT.jar'
            }
        }
        

    stage('Ansible Playbook') {
                steps {
    				echo 'Running Ansible Playbook...'
    				
                    ansiblePlaybook become: true,
                    disableHostKeyChecking: true,
                    inventory: 'vagrant/hosts',
                    playbook: 'vagrant/playbook1.yml'
                }
    }

    stage ('Docker Images') {
      steps {
        script {
            docker.withRegistry('https://hub.docker.com/', 'dockerid'){
            checkout scm
                    stage(‘images’) {
                      sh ‘docker-compose –f build-compose.yml run –rm compile’
                    }
            def customImage = docker.build("phil1020/phil-docker-repo:${env.BUILD_ID}", "smarthome_g6")
            customImage.push()
        }
      }
    }
    }
}
}
```

On the first stage, Checkout, the goal is to checkout the code from the repository. So, add the credentials and the URL to access Bitbucket repository. 

The second stage is Build, using `mvn clean package` on the project directory that takes the compiled code and packages it in its distributable format, in this case a JAR file.

Next, on the third stage - Javadoc - generate the Javadoc and also publish them. To do this, use the command `mvn javadoc:javadoc`. Additionally, configure all the necessary parameters for the `publishHTML` step:

- `reportName` parameter refers to the name of the report to display for the build/project
- `reportDir` parameter specifies the path to the HTML report directory relative to the workspace
- `reportFiles` parameter indicates the file to provide links inside the report directory
- `keepAll` parameter is set to false to only archive the most recent reports for successfull build
- `alwaysLinkToLastBuild` parameter is set to false so it does not publish the link on project level if build failed.
- `allowMissing` parameter is also set to false so it does not allow report to be missing and build will fail on missing report

At the fourth stage - Test - use `mvn test` to run the unit tests, the Junit plugin to publish them and the Jacoco plugin to include code coverage.

On the fifth stage - Archive - use `archiveArtifacts` to save the artifacts outside workspace.

The sixth stage is Docker Images, using a script to login on Docker Hub and publish the images generated by the Dockerfile on this repository.

An Ansible Playbook is executed at the end of the pipeline to deploy the new build/version of the SmartHome application.

## Docker
### Containers with Docker

Docker provides the ability to package and run an application in a loosely isolated environment called a container. The isolation and security allows you to run many containers simultaneously on a given host.

Containers are lightweight because they don't need the extra load of a hypervisor, but run directly within the host machine’s kernel. This means you can run more containers on a given hardware combination than if you were using virtual machines. You can even run Docker containers within host machines that are actually virtual machines.

### Multiple Docker Containers

For creating a containerized environment to execute the SmartHome application it is necessary to setup 3 containers to create isolated environments:
 - **db**: H2 server database
 - **backend**: SmartHome application
 - **frontend**: React application
 
Start by creating a folder for each container and name them as described above.
In the **backend** folder, place the SmartHome project .jar file and add the following Dockerfile in order to build the respective docker image:

```
# Uses Docker Official OpenJDK base image containing Java 8 runtime environment
FROM openjdk:8

#Container will list on port 8443 at runtime
EXPOSE 8443

#Next we make a directory for our app and set it as our working directory.
RUN mkdir -p /usr/src/app

WORKDIR /usr/sr/app/

#Copies jar from source into the container /usr/src/app/ folder
COPY /smarthome-1.0-SNAPSHOT.jar /usr/src/app/smarthome-1.0-SNAPSHOT.jar

#An ENTRYPOINT allows to configure a container that will run as an executable jar
ENTRYPOINT ["java","-jar","smarthome-1.0-SNAPSHOT.jar"]
```

In the **frontend** folder, place the REACT project "/build" folder to the "frontend" and add the following Dockerfile in order to build the respective docker image:

```
# base image
FROM node:10.16-alpine

# creating working directory
RUN mkdir -p /srv/app/smarthome-client

# set working directory
WORKDIR /srv/app/smarthome-client

# copy json files to working directory
COPY package.json /srv/app/smarthome-client
COPY package-lock.json /srv/app/smarthome-client

# install and cache app dependencies
RUN npm install --silent
RUN npm install react-scripts@3.0.1 -g --silent

COPY . /srv/app/smarthome-client

ENTRYPOINT [ "npm","start"]
```

In the **db** folder you will need only to add the following Dockerfile:

```
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget http://repo2.maven.org/maven2/com/h2database/h2/1.4.199/h2-1.4.199.jar

CMD java -cp ./h2-1.4.199.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists
``` 


In order to create a  a multi-container application you can use the Docker Compose tool which require that  you define the services that need to run in a YAML file. 
Therefore it was configured the following **docker-compose.yml** file:
```
version: '3'
services:
  db:
    build: db
    ports:
      - "9092:9092"
  backend:
    build: backend
    ports:
      - "8443:8443"
      - "8080:8080"
  frontend:
    build: frontend
    ports:
      - "3000:3000"
    volumes:
      - ./data:/usr/src/data
``` 

Afterwards, it will be possible to execute  ` docker-compose build` command in order to build all docker images at once. This task, however, will be executed through the pipeline  and published in docker hub as required.


## Ansible

Ansible is a simple automation engine that automates cloud provisioning, configuration management, application deployment, intra-service orchestration, etc. It models the infrastructure by describing how all of the systems inter-relate, rather than just managing one system at a time.
It uses no agents and no additional custom security infrastructure, so it's easy to deploy - and most importantly, it uses a very simple language (YAML, in the form of Ansible Playbooks) that allows you to describe automation jobs in a way that approaches plain English.

Playbooks can finely orchestrate multiple slices of the infrastructure topology, with very detailed control over how many machines to tackle at a time. 

### Configuration Management with Ansible

Use Ansible to manage the previous VMs by creating a playbook.yml inside the /vagrant folder:    

```
---
- hosts: otherservers
  become: yes
  tasks:
    - name: update apt cache
      apt: update_cache=yes
    - name: remove apache
      apt: name=apache2 state=absent
    - name: install jdk
      apt: name=openjdk-8-jdk-headless state=present    
- hosts: app
  become: yes
  tasks:
    - name: Copy the .jar file
      copy: remote_src=no src=vagrant/smarthome-1.0-SNAPSHOT.jar dest=/home/vagrant/smarthome-1.0-SNAPSHOT.jar mode=0755
    - name: run the app
      shell: nohup java -jar /vagrant/smarthome-1.0-SNAPSHOT.jar &
- hosts: db
  become: yes
  tasks:
    - name: Download h2 database from maven.org
      get_url: url=http://repo2.maven.org/maven2/com/h2database/h2/1.4.199/h2-1.4.199.jar dest=./h2-1.4.199.jar mode=0755
    - name: Run H2
      shell: java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > out.txt
      async: 10000
      poll: 0
```

Start all the VMs (according to the Vagrantfile configuration):
	
	$ vagrant up

Inside the “ansible” VM:

    $ vagrant ssh ansible
    
    
Then, inside the Ansible box lets change directory to /vagrant to find the files of our repository:


    $ cd vagrant


There is a file called hosts in the directory. This file will serve as the inventory file:


    [otherservers]
    app ansible_ssh_host=192.168.33.11 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/app/virtualbox/private_key
    db ansible_ssh_host=192.168.33.12 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/db/virtualbox/private_key
    ui ansible_ssh_host=192.168.33.13 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/ui/virtualbox/private_key


After, we created the ansible.cfg file in the current directory, alongside the playbook.
We use this file to setup some ansible defaults:


    [defaults]
    inventory = /vagrant/hosts
    remote_user = vagrant 



Execute the playbook:


    $ ansible-playbook playbook1.yml
    


This part of playbook is to be applied to the otherserver host group (see the hosts file). So, it will be applied to host1 (app) and host2 (db).
This play is to be executed as superuser.

    - hosts: otherservers
      become: yes
      tasks:
        - name: update apt cache
          apt: update_cache=yes
        - name: remove apache
          apt: name=apache2 state=absent
        - name: install jdk
          apt: name=openjdk-8-jdk-headless state=present 


This part of playbook is to be applied to App. 
We start a play that will be applied only to App, runnig the jar file of application:

    
        - hosts: app
          become: yes
          tasks:
            - name: Copy the .jar file
              copy: remote_src=no src=vagrant/smarthome-1.0-SNAPSHOT.jar dest=/home/vagrant/smarthome-1.0-SNAPSHOT.jar mode=0755
            - name: run the app
              shell: nohup java -jar /vagrant/smarthome-1.0-SNAPSHOT.jar &
        

This part of playbook is to be applied to H2 Database. 
This part of the script will be applied only to H2 Database, using the get_url module to download the H2 files to install the Database.

        - hosts: db
      become: yes
      tasks:
            - name: Download h2 database from maven.org
              get_url: url=http://repo2.maven.org/maven2/com/h2database/h2/1.4.199/h2-1.4.199.jar dest=./h2-1.4.199.jar mode=0755
            - name: Run H2
              shell: java -cp ./h2*.jar org.h2.tools.Server -web -webAllowOthers -tcp -tcpAllowOthers -ifNotExists > out.txt
              async: 10000
              poll: 0
    


As previously shown, a stage was added to the Jenkins Pipeline to executed this Ansible Playbook.

##### References:

[Introduction to Vagrant](https://www.vagrantup.com/intro/index.html)

[What is Git?](https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F)

[What does Build Tool mean?](https://www.techopedia.com/definition/16359/build-tool)

[What is Jenkins for Continuous Integration?](https://www.guru99.com/jenkin-continuous-integration.html#1)

[The Docker platform](https://docs.docker.com/engine/docker-overview/)

[How Ansible works](https://www.ansible.com/overview/how-ansible-works)
