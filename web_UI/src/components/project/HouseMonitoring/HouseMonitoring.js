

import React from "react";
import AreaMonitoringMenu from "./HouseArea/AreaMonitoringMenu";

class HouseMonitoring extends React.Component {
    render() {
        return (
            <>
                <div className="content">
                    <AreaMonitoringMenu/>
                </div>
            </>
        );
    }
}

export default HouseMonitoring;
